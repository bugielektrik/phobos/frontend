js_import sites-available/oauth2.js;

server
{
  server_name kyosk.autobaq.kz;

  gzip on;
  gzip_static on;
  gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
  gzip_proxied any;
  gzip_vary on;
  gzip_comp_level 6;
  gzip_buffers 16 8k;
  gzip_http_version 1.1;

  client_max_body_size 50m;

  location /api/v1/otps {
    proxy_pass http://127.0.0.1:9090/api/v1/otps;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/catalogs {
    proxy_pass http://127.0.0.1:9696/api/v1/catalogs;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/products {
    proxy_pass http://127.0.0.1:9696/api/v1/products;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/stores {
    proxy_pass http://127.0.0.1:9090/api/v1/stores;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/accounts {
    auth_request /_oauth2_token_introspection;

    auth_request_set $account_id $sent_http_token_account_id;
    proxy_set_header X-Account-ID $account_id;

    auth_request_set $account_type $sent_http_token_account_type;
    proxy_set_header X-Account-Type $account_type;

    proxy_pass http://127.0.0.1:9090/api/v1/accounts;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/addresses {
    auth_request /_oauth2_token_introspection;

    auth_request_set $account_id $sent_http_token_account_id;
    proxy_set_header X-Account-ID $account_id;

    auth_request_set $account_type $sent_http_token_account_type;
    proxy_set_header X-Account-Type $account_type;

    proxy_pass http://127.0.0.1:9393/api/v1/addresses;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/inventories {
    auth_request /_oauth2_token_introspection;

    auth_request_set $account_id $sent_http_token_account_id;
    proxy_set_header X-Account-ID $account_id;

    auth_request_set $account_type $sent_http_token_account_type;
    proxy_set_header X-Account-Type $account_type;

    proxy_pass http://127.0.0.1:9595/api/v1/inventories;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/orders {
    auth_request /_oauth2_token_introspection;

    auth_request_set $account_id $sent_http_token_account_id;
    proxy_set_header X-Account-ID $account_id;

    auth_request_set $account_type $sent_http_token_account_type;
    proxy_set_header X-Account-Type $account_type;

    proxy_pass http://127.0.0.1:9797/api/v1/orders;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location /api/v1/cards {
    auth_request /_oauth2_token_introspection;

    auth_request_set $account_id $sent_http_token_account_id;
    proxy_set_header X-Account-ID $account_id;

    auth_request_set $account_type $sent_http_token_account_type;
    proxy_set_header X-Account-Type $account_type;

    proxy_pass http://127.0.0.1:9898/api/v1/cards;
    proxy_read_timeout 900s;
    proxy_send_timeout 900s;
  }

  location = /_oauth2_token_introspection {
    internal;
    js_content oauth2.introspectAccessToken;
  }

  location = /_oauth2_send_request {
    internal;
    set $args $args&access_token=$http_authorization;

    proxy_method      GET;
    proxy_set_header  Content-Type "application/x-www-form-urlencoded";
    proxy_pass        http://127.0.0.1:9292/api/v1/oauths/token;
  }

  location /
  {
    root /usr/share/nginx/kyosk;
    index index.html index.htm;
    try_files $uri /index.html;
    etag off;
    proxy_set_header Cache-Control "max-age=3600, immutable";
  }

  error_page 404 /404.html;

  # redirect server error pages to the static page /50x.html
  #
  error_page 500 502 503 504 /50x.html;
  location = /50x.html
  {
    root /usr/share/nginx/kyosk;
  }

  listen 443 ssl; # managed by Certbot
  ssl_certificate /etc/letsencrypt/live/kyosk.autobaq.kz-0001/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/kyosk.autobaq.kz-0001/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server
{
  if ($host = kyosk.autobaq.kz)
  {
    return 301 https://$host$request_uri;
  }
  # managed by Certbot

  server_name kyosk.autobaq.kz;
  listen 80;
  return 404;
  # managed by Certbot
}
