import axios from 'axios';
import store from '@/store';

const headers = {
  'Content-Type': 'application/json'
};

const request = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
});

request.defaults.headers = headers;

request.interceptors.request.use((config) => {
  const accessToken = localStorage.getItem('token');
  if (accessToken) {
    if (config?.data?.needAuth) {
      console.log(config.data);
      delete config.data.needAuth;

      config.headers.Authorization = `${accessToken}`;
    } else if (config?.data?.needAuthWithBearer) {
      delete config.data.needAuthWithBearer;

      config.headers.Authorization = `Bearer ${accessToken}`;
    }
  }
  return config;
});

request.interceptors.response.use(
  (response) => response,
  function (error) {
    if (error?.response?.status === 401) {
      store.dispatch('logout');
    }
    return Promise.reject(error);
  }
)

export default request;