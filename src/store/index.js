import Vue from 'vue'
import Vuex from 'vuex';
import { requestOtp, requestCheckPinCode, 
  requestSendAddress, requestGetAddresses, 
  requestDeleteAddress, requestChooseMainAddress, 
  requestShops, requestInventories, requestProductById, requestProducts } from '../api/address';

import { saveCard, getCards } from '@/api/cards.js';

// import { createOrder } from '@/api/orders.js';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    accessToken: '',
    phone: '',
    key: '',
    isAuth: '',

    authMenu: false,

    addresses: [],
    shops: [],
    selectedShop: null,
    inventories: [],
    cards: [],

    basket: [],

    orderStatus: 'no',

    tips: {
      main: true,
      chooseAddress: true,
      addAddress: true,
      chooseCard: true,
      addCard: true,
      auth: true,
    }
  },
  getters: {
    addresses(state) {
      return state.addresses;
    },

    shops(state) {
      return state.shops;
    },

    selectedShop(state) {
      return state.selectedShop;
    },

    mainAddress(state) {
      return state.addresses.find((obj) => obj.is_main === true) ?? null;
    },

    mainCard(state) {
      return state.cards.find((obj) => obj.is_main === true) ?? null;
    },

    totalPrice(state) {
      if (state.basket.length) {
        let totalPrice = 0;

        state.basket.forEach((el) => { totalPrice += +el.price_actual * el.amount });

        return totalPrice;
      } else {
        return 0;
      }
    },

    totalAmount(state) {
      if (state.basket.length) {
        let totalAmount = 0;

        state.basket.forEach((el) => { totalAmount += el.amount });

        return totalAmount;
      } else {
        return 0;
      }
    }

  },
  mutations: {

    ADD_PRODUCT_IN_BASKET(state, { product }) {
      let findProduct = state.basket.find((el) => el.product_id === product.product_id);

      if (findProduct) {
        findProduct.amount += 1;
      } else {
        state.basket.push({ ...product, amount: 1 });
      }

      console.log(state.basket);
    },

    REMOVE_PRODUCT_FROM_BASKET(state, { productId }) {
      let findProduct = state.basket.find((el) => el.product_id === productId);

      if (findProduct) {
        if (findProduct.amount > 0) {
          findProduct.amount -= 1;
        }
        
      }
    },

    SET_PHONE(state, { phone }) {
      state.phone = phone;
    },

    SET_USER_KEY(state, { key }) {
      state.key = key;
    },

    SET_ACCESS_TOKEN(state, { token }) {
      state.accessToken = token;
    },

    SET_IS_AUTH(state, { isAuth }) {
      state.isAuth = isAuth;
    },

    SET_TIP_STATUS(state, { tip, status }) {
      state.tips[tip] = status;
    },

    SET_ADDRESSES(state, { addresses }) {
      state.addresses = addresses;
    },

    SET_CARDS(state, { cards }) {
      state.cards = cards;
    },

    SET_SHOPS(state, { shops }) {
      state.shops = shops;
    },

    SET_SHOP_INVENTORIES(state, { inventories }) {
      state.inventories = inventories;
    },

    SET_SELECTED_SHOP(state, { shop }) {
      state.selectedShop = shop;
    },

    TOGGLE_AUTH_MENU(state, { status }) {
      state.authMenu = status;
    }
  },
  actions: {

    logout({commit}) {
      commit('SET_IS_AUTH', { isAuth: false });
      commit('TOGGLE_AUTH_MENU', { status: true });
      commit('SET_PHONE', { phone: '' });
      commit('SET_ACCESS_TOKEN', { token: '' });
    },

    async GET_USER_KEY({commit}, { phone }) {
      try {
        console.log(phone);
        const result = await requestOtp(phone);
        if (result.status === 200) {
          commit('SET_PHONE', { phone });
          commit('SET_USER_KEY', { key: result.data.key });
          window.alert(result.data.otp);
        }
      } catch (e) {
        console.log(e);
      }
    },

    async CHECK_PIN_CODE({ state, commit, dispatch }, { code }) {
      const { phone, key } = state;
      try {
        if (!phone) return;
        const result = await requestCheckPinCode(phone, key, code);

        if (result.status === 200) {
          commit('SET_ACCESS_TOKEN', { token: result.data.access_token });
          commit('SET_IS_AUTH', { isAuth: true });
          commit('TOGGLE_AUTH_MENU', { status: false });

          localStorage.setItem('token', result.data.access_token);
          
          dispatch('GET_ADDRESSES');
          dispatch('getCards');
        }
      } catch (e) {
        console.log(e);
      }
    },

    async GET_ADDRESSES({ state, commit, dispatch }) {
      try {
        const result = await requestGetAddresses();

        commit('SET_ADDRESSES', { addresses: result });
        commit('SET_SHOPS', { shops: [] });

        let mainAddress = state.addresses.find((obj) => obj.is_main === true) ?? null;

        if (mainAddress) {
          dispatch('GET_SHOPS', { mainAddress });
        }
        
      } catch (e) {
        console.log(e);
      }
    },

    async SEND_ADDRESS({ dispatch }, { data }) {
      try {
        await requestSendAddress(data);

        dispatch('GET_ADDRESSES');
        
      } catch (e) {
        console.log(e);
      }
    },

    async DELETE_ADDRESS({ dispatch }, { id }) {
      try {
        await requestDeleteAddress(id);

        dispatch('GET_ADDRESSES');
        
      } catch (e) {
        console.log(e);
      }
    },

    async CHOOSE_MAIN_ADDRESS({ dispatch }, { data }) {
      try {
        const setMain = {...data, is_main: true};
        console.log('CHOOSE_MAIN_ADDRESS', setMain);
        const result = await requestChooseMainAddress(setMain);

        dispatch('GET_ADDRESSES');
        
        console.log(result);
      } catch (e) {
        console.log(e);
      }
    },

    async GET_SHOPS({ commit }, { mainAddress }) {
      try {
        const result = await requestShops(mainAddress.latitude, mainAddress.longitude);

        commit('SET_SHOPS', { shops: result });
        
      } catch (e) {
        console.log(e);
      }
    },

    async getShopInventories({ commit }) {
      try {
        const result = await requestInventories();

        // for (let i = 0; i < result.length; i++) {
        //   let product = await dispatch('getProductById', { id: result[i].product_id });
        //   result[i] = {...result[i], ...product, chosenAmount: 0};
        // }

        commit('SET_SHOP_INVENTORIES', { inventories: result });
        
        // console.log(JSON.stringify(result));
      } catch (e) {
        console.log(e);
      }
    },

    async getProductById(_, { id }) {
      try {
        const result = await requestProductById(id);

        return result;
      } catch (e) {
        console.log(e);
      }
    },

    async getProducts({commit}, { shopID }) {
      try {
        const result = await requestProducts(shopID);

        commit('SET_SHOP_INVENTORIES', { inventories: result });

      } catch (e) {
        console.log(e);
      }
    },

    async saveCard({ dispatch }, { data }) {
      try {
        await saveCard(data);

        dispatch('getCards');
        
      } catch (e) {
        console.log(e);
      }
    },

    async getCards({ commit }) {
      try {
        const result = await getCards();

        commit('SET_CARDS', { cards: result });
        
      } catch (e) {
        console.log(e);
      }
    },

    async deleteCard({ dispatch }, { id }) {
      try {
        await requestDeleteAddress(id);

        dispatch('getCards');
      
      } catch (e) {
        console.log(e);
      }
    },

    setSelectedShop({ commit, dispatch }, { shop }) {
      commit('SET_SELECTED_SHOP', { shop: shop });

      commit('SET_SHOP_INVENTORIES', { inventories: [] });

      dispatch('getProducts', { shopID: shop.id });
    },

    // async createOrder() {
    //   try {
    //     await createOrder(id);

    //     dispatch('getCards');
      
    //   } catch (e) {
    //     console.log(e);
    //   }
    // }

  },
  modules: {
  }
})

export default store;