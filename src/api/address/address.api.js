import axios from "axios";
import { request } from '../../utils';

const headers = {
  'Content-Type': 'application/json'
};

export const requestOtp = async (phone = null) => {
  if (phone) {
    const params = { phone };
    console.log('masd');
    return await axios.get(process.env.VUE_APP_BASE_URL + `/otps?phone=${phone}&debug=true`, { headers, params });
  }
};

export const requestCheckPinCode = async (phone = null, key = null, code = null) => {
  if (phone && key && code) {
    return await axios.post(
      process.env.VUE_APP_BASE_URL + '/otps',
      { phone, key, otp: code },
      { headers: headers },
    );
  }
};

export const requestSendAddress = async (data) => {
  const response = await request.post('/addresses', {
    ...data,
    needAuth: true
  });

  return response.data;
  
}

export const requestGetAddresses = async () => {
  const response = await request.get('/addresses', {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export const requestDeleteAddress = async (id) => {
  const response = await request.delete('/addresses/' + id, {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export const requestChooseMainAddress = async (data) => {
  const response = await request.put('/addresses/' + data.address_id, {
    ...data,
    needAuth: true
  });

  return response.data;
}

export const requestShops = async (latitude, longitude) => {
  const response = await request.get(`/stores?latitude=${latitude}&longitude=${longitude}`, {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export const requestInventories = async () => {
  const response = await request.get('/inventories?store_id=605755cd-2dc8-4dc8-8919-6dbbec3a4459', {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export const requestProducts = async (shopID) => {
  const response = await request.get(`/inventories?store_id=${shopID}`, {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export const requestProductById = async (id) => {
  const response = await request.get('/products/' + id, {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export default { requestOtp, requestCheckPinCode, requestSendAddress, requestGetAddresses, requestDeleteAddress, requestChooseMainAddress, requestShops, requestInventories, requestProductById };