import { request } from '@/utils';

export const saveCard = async (data) => {
  const response = await request.post('/cards', {
    ...data,
    needAuth: true
  });

  return response.data;
}

export const getCards = async () => {
  const response = await request.get('/cards', {
    data: {
      needAuth: true
    },
  });

  return response.data;
}

export const deleteCard = async (id) => {
  const response = await request.delete('/cards/' + id, {
    data: {
      needAuth: true
    },
  });

  return response.data;
}