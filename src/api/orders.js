import { request } from '@/utils';

export const createOrder = async (data) => {
  const response = await request.post('/orders', {
    ...data,
    needAuth: true
  });

  return response.data;
}