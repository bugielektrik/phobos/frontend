import Vue from 'vue';
import VueRouter from 'vue-router';
import ViewGoods from '@/views/ViewGoods.vue';
import ViewMain from '@/views/ViewMain.vue';
import ViewAddressChoose from '@/views/ViewAddressChoose.vue';
import ViewAddressAdd from '@/views/ViewAddressAdd.vue';
import ViewShopChoose from '@/views/ViewShopChoose.vue';
import ViewProductsChoose from '@/views/ViewProductsChoose.vue';
import ViewCardChoose from '@/views/ViewCardChoose.vue';
import ViewCardAdd from '@/views/ViewCardAdd.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'main',
    component: ViewMain,
    children: [
      {
        path: 'choose-address',
        name: 'choose-address',
        component: ViewAddressChoose,
      },
      {
        path: 'add-address',
        name: 'add-address',
        component: ViewAddressAdd,
      },
      {
        path: 'choose-shop',
        name: 'choose-shop',
        component: ViewShopChoose,
      },
      {
        path: 'choose-products',
        name: 'choose-products',
        component: ViewProductsChoose,
      },
      {
        path: 'choose-card',
        name: 'choose-card',
        component: ViewCardChoose,
      },
      {
        path: 'add-card',
        name: 'add-card',
        component: ViewCardAdd,
      }
    ]
  },
  {
    path: '/goods',
    name: 'Goods',
    component: ViewGoods
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
